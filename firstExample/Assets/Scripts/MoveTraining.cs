using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MoveTraining : MonoBehaviour
{
    public float forceValue;
    public float jumpValue;

    private AudioSource audioSource;
    private Rigidbody rb;

    private int life = 5;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
    
    }
    
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            print("collision");
            Destroy(collision.gameObject);
            audioSource.Play();
        }
    }
    private void FixedUpdate()
    {
        rb.AddForce(new Vector3(Input.GetAxis("Horizontal"),
            0,
            Input.GetAxis("Vertical")) * forceValue);
        rb.AddForce(new Vector3(Input.acceleration.x,
            0,
            Input.acceleration.y) * forceValue);
    }
    
    private void OnTriggerEnter(Collider other)
    {
        print("META");
        SceneManager.LoadScene("Awake");

    }
}