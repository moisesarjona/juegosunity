using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpulseLvl2 : MonoBehaviour
{
    public GameObject proyectil;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Throwproyectile());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    IEnumerator Throwproyectile()
    {
        int time = Random.Range(1, 10);
        
        yield return new WaitForSeconds(time);
        
        while (true)
        {
            time = Random.Range(1, 10);
            
            Instantiate(proyectil, transform.position, Random.rotation);
            
            yield return new WaitForSeconds(time);
        }
    }
}
