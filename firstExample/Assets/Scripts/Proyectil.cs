using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proyectil : MonoBehaviour
{
    private float impulseForce = 10.0f;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Rigidbody>().angularDrag = 0;
        GetComponent<Rigidbody>().AddForce(Vector3.back * impulseForce, ForceMode.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.z < -190.0f || transform.position.y < -1.0f)
        {
            Destroy(gameObject); 
        }
    }
}
