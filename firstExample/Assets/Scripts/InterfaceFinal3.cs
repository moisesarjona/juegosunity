using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InterfaceFinal3 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void ClickRestart()
    {
        SceneManager.LoadScene("Level2");
    }
    
    public void ClickNextLevel()
    {
        SceneManager.LoadScene("Level3");
    }
    
    public void ClickInicio()
    {
        SceneManager.LoadScene("Awake");
    }

}