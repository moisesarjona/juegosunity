using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MoveLvl3 : MonoBehaviour
{
    public float forceValue;

    private AudioSource audioSource;
    private Rigidbody rb;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        GameManager.vidas = 5;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void FixedUpdate()
    {
        rb.AddForce(new Vector3(Input.GetAxis("Horizontal"),
            0,
            Input.GetAxis("Vertical")) * forceValue);
        rb.AddForce(new Vector3(Input.acceleration.x,
            0,
            Input.acceleration.y) * forceValue);
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            print("collision");
            Destroy(collision.gameObject);
            GameManager.vidas -= 1;
            audioSource.Play();
            if (GameManager.vidas == 0)
            {
                print("THE SPHERE IS DEAD");
                SceneManager.LoadScene("FinalPerdido");
            }
            print("The life os the sphere is " + GameManager.vidas);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        print("META");
        SceneManager.LoadScene("FinalGanado");

    }
}
