using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceVidas : MonoBehaviour
{
    public Text vidas;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        vidas.text = "Vidas restantes = " + GameManager.vidas;
    }
}
